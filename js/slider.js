const rangeFrom = document.querySelector('#price-range-from');
const rangeTo = document.querySelector('#price-range-to');

const rangeFromLabel = document.querySelector('#price-range-from-value');
const rangeToLabel = document.querySelector('#price-range-to-value');

const rangeInput = rangeFrom;
const getGradientBg = (colorStopFrom, colorStopTo) => {
  const backTrackColor = 'rgba(255, 255, 255, 0.5)';

  return `linear-gradient(to right,
    ${backTrackColor} ${colorStopFrom / 10}%,
    white ${colorStopFrom / 10}%, white ${colorStopTo / 10}%,
    ${backTrackColor} ${colorStopTo / 10}%)`;
}

rangeFrom.oninput = () => {
  fromVal = +rangeFrom.value;
  toVal = +rangeTo.value;

  if (fromVal + 5 >= toVal) {
    rangeFrom.value = toVal - 5;
  }

  rangeFromLabel.textContent = rangeFrom.value;
  rangeInput.style.backgroundImage = getGradientBg(rangeFrom.value, rangeTo.value);
};

rangeTo.oninput = () => {
  fromVal = +rangeFrom.value;
  toVal = +rangeTo.value;

  if (toVal - 5 <= fromVal) {
    rangeTo.value = fromVal + 5;
  }

  rangeToLabel.textContent = rangeTo.value;
  rangeInput.style.backgroundImage = getGradientBg(rangeFrom.value, rangeTo.value);
};