const promoSlides = [
  { styleClass: 'first-promo-slide' },
  { styleClass: 'second-promo-slide' },
  { styleClass: 'third-promo-slide' }
];

const sliderControls = Array.from(document.querySelector('.slider-control-list').children);
let currentControl = document.querySelector('.slider-control-item-current');
let currentPromoSlide = promoSlides[0];

sliderControls.forEach((control, index) => {
  control.onclick = () => {
    currentControl.classList.remove('slider-control-item-current');
    currentControl = control;
    currentControl.classList.add('slider-control-item-current');

    document.body.classList.remove(currentPromoSlide.styleClass);
    currentPromoSlide = promoSlides[index];
    document.body.classList.add(currentPromoSlide.styleClass);
  }
})


const feedbackModalTrigger = document.querySelector('#feedback-modal-trigger');
const feedbackModal = document.querySelector('.feedback-modal');
const modalOverlay = document.querySelector('.modal-overlay');

feedbackModalTrigger.onclick = () => {
  feedbackModal.style.display = 'block';
  modalOverlay.style.display = 'block';
};

const feedbackModalCloseBtn = document.querySelector('.feedback-modal .modal-close-btn');
feedbackModalCloseBtn.onclick = () => {
  feedbackModal.style.display = 'none';
  modalOverlay.style.display = 'none';
};